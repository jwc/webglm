"""
run_webglm.py run Flask app defined in webglm.py
"""

from webglm.main import app

if __name__ == "__main__":
    print("REMINDER: Is the RabbitMQ instance running?")
    print("REMINDER: Are the Celery workers running?")
    app.run(debug=False, host='0.0.0.0')
