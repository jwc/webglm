"""
app.py contains the Flask app object
"""

import os
import logging
import inspect

import flask as fl


# Change Flask's jinja2 options
class FlaskWithJinjaTrimEnabled(fl.Flask):
    jinja_options = fl.Flask.jinja_options.copy()
    jinja_options.update({
        "trim_blocks": True,
        "lstrip_blocks": True
    })


# Flask application instance
app = FlaskWithJinjaTrimEnabled(__name__)

# Logger object
logger = logging.getLogger(__name__ + '-logger')
logger.setLevel(logging.DEBUG)

# Log Formatter object
log_formatter = logging.Formatter(
    '%(asctime)s - %(clientip)s - %(levelname)s'
    ' - %(pymod)s:%(pyline)d - %(message)s',
    '%Y-%m-%d %H:%M:%S'
)

# application default configuration
app.config.update({
    "DEBUG": False,
    "DATABASE_TYPE": "sqlite",
    "DATABASE": os.path.join(app.root_path, "db/webglm.db"),
    "LOG": os.path.join(app.root_path, "logs/webglm.log"),
    "WORKER_LOG": os.path.join(app.root_path, "logs/worker.log"),
    "SECRET_KEY": "default_key",
    "CELERY_BROKER_URL": "amqp://guest@localhost//",
    "CELERY_BACKEND_URL": "amqp://guest@localhost//",
    "FILE_ENCODING": "utf-8",
    "MAX_CONTENT_LENGTH": 1024 * 1024,
    "MAX_STEPS": 20000
})

# overwrite default configuration with settings from file
app.config.from_pyfile('config/config.py', silent=False)


def notify_log(message, dest=print, request=None, level='info'):
    """Notify a callable of a message and then log it"""

    if isinstance(level, str):
        levels = {
            'debug': logging.DEBUG,
            'info': logging.INFO,
            'warning': logging.WARNING,
            'error': logging.ERROR,
            'critical': logging.CRITICAL
        }

        level = levels[level.lower()]

    # notify
    if dest is not None:
        dest(message)

    # log
    caller = inspect.getframeinfo(inspect.stack()[1][0])
    client_ip = request.remote_addr if request is not None else 'Not available'

    logger.log(
        level,
        message,
        extra={
            'clientip': client_ip,
            'pymod': caller.filename.split('/')[-1],
            'pyline': caller.lineno
        }
    )

    return
