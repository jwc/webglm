"""
glmdata.py routines and constants for specifying generalised linear models
"""

# Name of intercept field in JSON strings
INTERCEPT_NAME = "!intercept!"

# variable types
VARIABLE_TYPE = {
    "metric": "metric",
    "categorical": "categorical"
}

# likelihood function options for each type of variable
LIKELIHOOD_FUNCTIONS = {
    VARIABLE_TYPE['metric']: 'normal',
    VARIABLE_TYPE['categorical']: 'categorical'
}

# available step methods
STEP_METHODS = {
    "metropolis": "Metropolis-Hastings",
    "pymc3": "PyMC3 Automatic Assignment",
    "_default_key": "metropolis"
}


def df_to_var_types(df):
    """Infer variable types of columns in pandas DataFrame

    returns a list of (variable name, variable type) tuples
    """

    # possible variable types
    variables = []
    for var in df.columns:
        if df[var].dtype.name == 'category':
            var_type = VARIABLE_TYPE['categorical']
        elif df[var].dtype == 'float':
            var_type = VARIABLE_TYPE['metric']
        variables.append((var, var_type))

    return variables
