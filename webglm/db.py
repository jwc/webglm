"""
db.py Database specification and objects for webglm
"""

import peewee as pw
import playhouse.sqlite_ext as pwsqlite_ext

import webglm.app as wglm


# database object
if wglm.app.config['DATABASE_TYPE'] == "sqlite":
    db = pwsqlite_ext.SqliteExtDatabase(
        wglm.app.config['DATABASE'],
        pragmas=[('foreign_keys', 'ON')]
    )
elif wglm.app.config['DATABASE_TYPE'] == "postgres":
    raise NotImplementedError(
        'Postgres is currently unsupported'
    )
else:
    raise ValueError(
        'DATABASE_TYPE configuration setting "' +
        str(wglm.app.config['DATABASE_TYPE']) +
        '" is not recognised'
    )


# Data model starts
class BaseModel(pw.Model):
    protected = pw.BooleanField(default=False)

    class Meta:
        database = db


class File(BaseModel):
    """Represents a CSV file, also holds a pickled pandas DataFrame"""

    id = pw.PrimaryKeyField()
    file_name = pw.TextField()
    csv = pw.BlobField()
    df_pickle = pw.BlobField(null=True)
    file_status = pw.TextField()
    update_time = pw.TimestampField()

    class Meta:
        db_table = "files"


class Model(BaseModel):
    """Represents a GLM for a specific CSV file"""

    id = pw.PrimaryKeyField()
    file = pw.ForeignKeyField(
        File,
        related_name='models',
        on_delete='cascade'
    )
    model_name = pw.TextField()
    response_json = pw.TextField()
    predictors_json = pw.TextField()
    update_time = pw.TimestampField()

    class Meta:
        db_table = "models"


class Result(BaseModel):
    """Represents a MCMC job and also holds the results"""
    id = pw.PrimaryKeyField()
    model = pw.ForeignKeyField(
        Model,
        related_name='results',
        on_delete='cascade'
    )
    steps = pw.IntegerField()
    burn_steps = pw.IntegerField()
    thinning_factor = pw.IntegerField()
    step_type = pw.TextField()
    graph_json = pw.TextField(null=True)
    job_status = pw.TextField()
    celery_task_id = pw.TextField(null=True)
    update_time = pw.TimestampField()

    class Meta:
        db_table = "results"

# Data model ends


# possible states for various webglm record types
RECORD_STATUS = {
    "NEW": "WAIT",
    "OK": "OK",
    "BAD": "FAILED",
    "BUSY": "BUSY",
    "COMPLETED": "COMPLETED"
}
