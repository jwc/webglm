"""
worker.py code for the MCMC background task
"""

import json
import logging
import pickle

import celery as cel
import celery.utils.log as cel_log
import matplotlib
matplotlib.use('Agg')  # must be before matplotlib references
import matplotlib.pyplot as plt
import mpld3
import numpy as np
import pandas as pd
import peewee as pw
import pymc3 as pm
import pymc3.sampling as pm_sampling
import theano.tensor as tt

import webglm.app as wglm
import webglm.db as wglmdb
import webglm.glmdata as glm

# Celery application instance
celery = cel.Celery(
    wglm.app.name,
    broker=wglm.app.config['CELERY_BROKER_URL'],
    backend=wglm.app.config['CELERY_BACKEND_URL']
)

# Celery logger object
logger = cel_log.get_task_logger(__name__)


# Celery task object
@celery.task(bind=True)
def do_MCMC(self, result_id):

    # configure file logging
    log_formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(message)s',
        '%Y-%m-%d %H:%M:%S'
    )

    file_handler = logging.FileHandler(wglm.app.config['WORKER_LOG'])
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(log_formatter)

    logger.addHandler(file_handler)

    logger.info(
        'Performing MCMC Simulation for job "{}"'
        .format(result_id)
    )

    # connect db
    wglmdb.db.connect()
    try:
        runner = WebglmMCMCRunner(
            db=wglmdb.db,
            result_id=result_id
        )
        runner.run(cel_task=self)
    except WebglmError as e:  # catch and report webglm errors
        logger.info(e.args[0])
        runner.fail_job()
    except:  # report and reraise non-webglm errors
        logger.exception(
            'A non-WebglmError exception occured for job with result_id={}'
            .format(result_id)
        )
        runner.fail_job()
        raise
    else:
        logger.info(
            'Results computed and stored for result_id={}'
            .format(result_id)
        )
    finally:
        # shutdown file logging
        file_handler.flush()
        file_handler.close()
        logger.removeHandler(file_handler)

        # close db
        wglmdb.db.close()

    return


class WebglmError(Exception):
    """An Exception subclass representing errors originating in webglm"""

    pass


class WebglmJobUnit:
    """Class that represents a webglm File/Model/Result combination"""

    def __init__(self, result_id):
        self.file = None
        self.model = None
        self.result = None

        self._load_result_model_file_records(result_id=result_id)

    def _load_result_model_file_records(self, result_id):
        """Takes a result.id and returns corresponding database records"""

        # Is there a result? Return it if so.
        try:
            self.result = wglmdb.Result.get(wglmdb.Result.id == result_id)
            self.model = self.result.model
            self.file = self.model.file
        except pw.DoesNotExist:
            raise WebglmError(
                'Job (id: {}) does not exist'
                .format(result_id)
            )

        return

    def protect_and_save(self):
        """Set the record protected flags to True and save"""

        records = (self.result, self.model, self.file)

        # protect and save all objects
        for record in records:
            if hasattr(record, 'protected'):
                record.protected = True
            record.save()

        return


class WebglmMCMCRunner:
    """Class that encapsulates webglm MCMC operation"""

    def __init__(self, db, result_id):

        # lock database to ensure job is in a consistent state
        db.begin('exclusive')
        try:
            # get job data from database
            self.job = WebglmJobUnit(result_id)
            # result record must be 'NEW' for computation to be valid
            if self._result.job_status != wglmdb.RECORD_STATUS['NEW']:
                raise WebglmError(
                    'Job ID: {}, state is "{}", not "NEW"'
                    .format(self._result.id, self._result.job_status)
                )
        except:
            db.rollback()
            raise
        else:
            # Set the result and task statuses to busy
            self._result.job_status = wglmdb.RECORD_STATUS['BUSY']
            self._result.save()
            db.commit()

        # response data in pandas Series
        self._resp_series = None

        # predictors data in pandas DataFrame
        self._pred_df = None

        # model parameters in dict
        self._params = None

        # PyMC3 model object
        self._pymc_model = None

        # PyMC3 trace object
        self._pymc_trace = None

    # properties to delegate file, model and result to WebglmJobUnit
    @property
    def _file(self):
        return self.job.file

    @property
    def _model(self):
        return self.job.model

    @property
    def _result(self):
        return self.job.result

    def run(self, cel_task=None):
        """Load GLM from db, perform MCMC, plot and save results"""

        if cel_task:
            cel_task.update_state(state='CONFIGURATION')

        # create PyMC3 model
        self._create_pymc_model()

        # now do MCMC sampling
        self._perform_MCMC_sampling(cel_task)

        # create dict of plots to display
        if cel_task:
            cel_task.update_state(state='PLOTS')
        self._create_plots_and_save()

        return

    def _create_pymc_model(self):
        """Create PyMC3 Model object according to model configuration"""

        # unpickle pandas DataFrame object
        try:
            df = pickle.loads(self._file.df_pickle)
        except pickle.UnpicklingError:
            raise WebglmError(
                'UnpicklingError in pymc3.sample for result_id={}. '
                .format(self._result.id)
            )

        # get response and predictors data and model parameters
        self._create_resp_pred_params(df)

        # create and configure MCMC model to sample
        likelihood_type = self._params['likelihood']
        if likelihood_type == 'categorical':
            self._create_categorical_pymc_model()
        elif likelihood_type == 'normal':
            self._create_metric_pymc_model()
        else:
            message = (
                'Currently implemented likelihood types are: "categorical"'
                ' and "normal"... "{}" provided for model_id={}.'
                .format(likelihood_type, self._model.id)
            )
            raise WebglmError(message)

        return

    def _create_resp_pred_params(self, df):
        """Split predictor/response data in DataFrame according to model"""

        # collect model specification JSON and put into dicts
        response_dict = json.loads(self._model.response_json)
        predictors_dict = json.loads(self._model.predictors_json)
        intercept_dict = predictors_dict.pop(glm.INTERCEPT_NAME)

        # response variable name
        response, = response_dict.keys()
        likelihood_type, = response_dict.values()

        # create pandas series for response variable
        self._resp_series = df[response]

        # create pandas DataFrame for predictor variables including
        # dummy variables for any categorical predictors.
        self._pred_df = df[[pred for pred in predictors_dict]]
        self._pred_df = pd.get_dummies(self._pred_df, drop_first=True)

        # extract model parameters from specification
        self._params = {
            'mean': intercept_dict['normal']['mean'],
            'sd': intercept_dict['normal']['sd'],
            'likelihood': likelihood_type
        }

        return

    def _create_categorical_pymc_model(self):
        """Create PyMC3 Model object from response and predictor data"""

        # series of integer labels for response categories
        resp_ints_series = self._resp_series.cat.codes

        # number of categories, predictors and samples
        n_labels = len(self._resp_series.cat.categories)
        n_preds = len(self._pred_df.columns)
        n_samples = len(self._pred_df)

        with pm.Model() as self._pymc_model:
            mu = self._params['mean']
            sd = self._params['sd']

            # intercept prior for each category
            intercepts_nonzero = pm.Normal(
                'intercepts_',
                mu=mu,
                sd=sd,
                shape=n_labels-1,
                testval=np.random.randn(n_labels-1)
            )

            # fix first category intercept to zero for uniqueness
            intercepts = tt.concatenate(
                [np.zeros((1,)), intercepts_nonzero]
            )

            # coefficient priors for each category
            coefficients_nonzero = pm.Normal(
                'coefficients_',
                mu=mu,
                sd=sd,
                shape=(n_preds, n_labels-1),
                testval=np.random.randn(n_preds, n_labels-1)
            )

            # fix first category coefficients to zero for uniqueness
            coefficients = tt.concatenate(
                [np.zeros((n_preds, 1)), coefficients_nonzero],
                axis=1
            )

            # softmax the weighted predictors
            category_probs = tt.nnet.softmax(
                intercepts + tt.dot(self._pred_df.values, coefficients)
            )

            # likelihood is a categorical distribution with probabilities
            # corresponding to softmax of weighted predictors
            likelihood = pm.Categorical(
                self._resp_series.name,
                p=category_probs,
                observed=resp_ints_series
            )

            # label individual coefficients according to associated
            # predictor and target label
            for j, label in enumerate(
                self._resp_series.cat.categories
            ):
                pm.Deterministic(
                    'beta_0__label_' + str(label),
                    intercepts[j]
                )
                for i, pred_name in enumerate(self._pred_df):
                    pm.Deterministic(
                        'beta_' + str(pred_name) +
                        '__label_' + str(label),
                        coefficients[i, j]
                    )

        return

    def _create_metric_pymc_model(self):
        """Create PyMC3 Model object from response and predictor data"""

        # number of predictors
        n_preds = len(self._pred_df.columns)
        n_samples = len(self._pred_df)

        with pm.Model() as self._pymc_model:
            # priors currently use identical distributions
            mu = self._params['mean']
            sd = self._params['sd']

            # intercept prior
            intercept = pm.Normal(
                'beta_0',
                mu=mu,
                sd=sd,
                testval=np.random.randn()
            )

            # predictor coefficient priors
            coefficients = pm.Normal(
                'coefficients_',
                mu=mu,
                sd=sd,
                shape=(n_preds, 1),
                testval=np.random.randn(n_preds, 1)
            )

            # arbitrary likelihood parameters
            noise_sd = pm.HalfCauchy(
                'noise_sd',
                beta=10.0,
                testval=np.abs(10.0*np.random.standard_cauchy())
            )

            # likelihood predictor function
            likelihood_mean = (
                tt.tile(intercept, (n_samples, 1)) +
                tt.dot(self._pred_df.values, coefficients)
            )

            # likelihood
            likelihood = pm.Normal(
                self._resp_series.name,
                mu=likelihood_mean,
                sd=noise_sd,
                observed=self._resp_series.values[None].T
            )

            # label individual coefficients according to associated predictor
            # and target label
            for i, pred_name in enumerate(self._pred_df.columns):
                pm.Deterministic(
                    'beta_' + str(pred_name),
                    coefficients[i, 0]
                )

        return

    def _perform_MCMC_sampling(self, cel_task=None):
        """Carry out MCMC sampling of PyMC3 model object"""

        UPDATE_FREQ_STEPS = 100

        # MCMC starting point
        start = pm.find_MAP(model=self._pymc_model)

        # assign step types: either metropolis or let pymc choose
        if self._result.step_type == 'metropolis':
            step = pm.Metropolis(model=self._pymc_model)
        else:
            with self._pymc_model:  # pymc needs this at the moment
                step = pm_sampling.assign_step_methods(self._pymc_model)

        # do sampling with periodic updates to celery task progress indicator
        for i, self._pymc_trace in enumerate(
            pm.iter_sample(
                self._result.steps,
                start=start,
                step=step,
                model=self._pymc_model
            ),
            start=1
        ):
            if cel_task and i % UPDATE_FREQ_STEPS == 1:
                cel_task.update_state(
                    state='MCMC',
                    meta={
                        'current': i,
                        'total': self._result.steps
                    }
                )

        return

    def _create_plots_and_save(self):
        """Generate a dictionary of html plots for display on results page"""

        # which parts of the trace to use
        burn_steps = self._result.burn_steps
        thinning_factor = self._result.thinning_factor

        # can display any number of plots on the results page by adding
        # (name, mpld3 json) pairs to the plots dict.
        plots = {}

        # Plot posterior distribution histogram and convert to JSON with mpld3
        ax = pm.plot_posterior(
            self._pymc_trace[burn_steps::thinning_factor],
            color='#0a3956',
            alpha=0.5
        )

        figure = ax.flat[0].figure
        figure.subplots_adjust(left=0.1)

        plots['Histograms'] = json.dumps(
            mpld3.fig_to_dict(figure)
        )

        self._result.graph_json = json.dumps(plots)
        self._result.job_status = wglmdb.RECORD_STATUS['COMPLETED']
        self._result.save()

        return

    def fail_job(self):
        """Set the status of the job record to indicate failure and save"""

        if self.job:
            self._result.job_status = wglmdb.RECORD_STATUS['BAD']
            self.job.protect_and_save()

        return
