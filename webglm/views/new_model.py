"""
new_model.py routines for handling model creation for webglm web-app
"""

import io
import json
import pickle
import keyword

import flask as fl
import peewee as pw
import wtforms as wtf
import pandas as pd

import webglm.app as wglm
import webglm.db as wglmdb
import webglm.glmdata as glm


@wglm.app.route('/new_model/<int:file_id>', methods=['GET', 'POST'])
def web_new_model(file_id):
    # get file record and pandas DataFrame by file_id
    try:
        file, df = _get_file_df_from_file_id(
            file_id
        )
    except ValueError as e:
        wglm.notify_log(
            e.args[0],
            dest=fl.flash, request=fl.request
        )

        return fl.redirect(
            fl.url_for('web_view_index')
        )

    # create WTForm Form object and configure according to given df
    form = _create_form_from_df_request(
        df=df,
        request=fl.request
    )

    # POST request with valid form
    if fl.request.method == 'POST' and form.validate():
        # create new db.Model object
        model = _create_model_from_form(
            form=form,
            file_id=file_id,
            vars=glm.df_to_var_types(df)
        )
        model.save()

        wglm.notify_log(
            'Created the model "{}".'
            .format(model.model_name),
            dest=fl.flash, request=fl.request
        )

        return fl.redirect(
            fl.url_for('web_view_index')
        )

    else:  # GET request or POSTed form didn't validate
        # make a dictionary of variable options
        response_opts = {opt.label.text: opt for opt in form.response}
        predictors_opts = {opt.label.text: opt for opt in form.predictors}

        # flash any errors in the form
        if len(form.errors) > 0:
            _flash_form_errors(form=form)

        return fl.render_template(
            'new_model.html',
            variables=glm.df_to_var_types(df),
            responses=response_opts,
            predictors=predictors_opts,
            form=form,
            filename=file.file_name,
            file_id=file_id,
            df_preview=df.head().to_html(
                classes="table table-bordered table-condensed"
            )
        )


class ModelForm(wtf.Form):
    """A WTForms class for webglm model creation forms"""

    model_name = wtf.fields.StringField(
        'Model Name',
        [wtf.validators.DataRequired(
            'Enter a model name'
        )],
        default="A New Model"
    )

    response = wtf.fields.RadioField(
        'Response',
        [wtf.validators.DataRequired(
            'Choose a response variable'
        )],
        choices=[]
    )

    predictors = wtf.fields.SelectMultipleField(
        'Predictors',
        [wtf.validators.DataRequired(
            'Choose at least one predictor variable'
        )],
        widget=wtf.widgets.ListWidget(prefix_label=False),
        option_widget=wtf.widgets.CheckboxInput(),
        choices=[]
    )

    prior_mean = wtf.fields.FloatField(
        'Mean',
        [wtf.validators.InputRequired(
            'Set a value for the prior mean'
        )],
        default="0.0"
    )

    prior_sd = wtf.fields.FloatField(
        'SD',
        [wtf.validators.InputRequired(
            'Set a value for the Standard Deviation'
        )],
        default="1.0e6"
    )

    protected = wtf.fields.BooleanField(
        'Protect from deletion',
        default=False
    )

    def validate_response(form, field):
        if field.data in form.predictors.data:
            raise wtf.ValidationError(
                str(field.data) +
                ' cannot be both predictor and response'
            )

    def validate_prior_sd(form, field):
        if field.data <= 0.0:
            raise wtf.ValidationError(
                'prior sd must be greater than zero'
            )


def _get_file_df_from_file_id(file_id):
    """Accepts a file_id and returns file record object and pd DataFrame

    DataFrame comes directly from csv data on first read, subsequent reads
    come from pickled object in database field."""

    # find File record with file_id
    try:
        file = wglmdb.File.get(wglmdb.File.id == file_id)
    except pw.DoesNotExist:
        raise ValueError(
            'No file with the id "{}" exists.'
            .format(file_id)
        )

    # Load CSV data and store as pickle if file_status is 'NEW',
    if file.file_status == wglmdb.RECORD_STATUS['NEW']:
        try:
            df = _create_df_from_file(
                file
            )
        except ValueError as e:
            file.file_status = wglmdb.RECORD_STATUS['BAD']
            raise ValueError(
                e.args[0]
            )
        else:
            # df created without exception, update status to OK
            file.df_pickle = pickle.dumps(df)
            file.file_status = wglmdb.RECORD_STATUS['OK']
        finally:
            file.save()

    # else file should have been previously pickled, so load it
    elif file.file_status == wglmdb.RECORD_STATUS['OK']:
        try:
            df = pickle.loads(file.df_pickle)
        except pickle.UnpicklingError:
            file.file_status = wglmdb.RECORD_STATUS['BAD']
            file.protected = True
            file.save()
            raise ValueError(
                'File record (id {}) cannot be unpickled. '
                'Protecting.'
                .format(file_id)
            )

    # file status is neither 'NEW' nor 'OK'
    else:
        raise ValueError(
            'Cannot configure model for file (id {})'
            ' with status {}.'
            .format(file_id, file.file_status)
        )

    return file, df


def _create_df_from_file(file_record):
    """Take File instance csv and return a pandas DataFrame"""

    FILE_ENCODING = wglm.app.config['FILE_ENCODING']

    if not isinstance(file_record, wglmdb.File):
        raise TypeError(
            str(type(file_record)) +
            ' object is not of type ' +
            str(wglmdb.File)
        )

    try:
        # get csv string from File object
        file_str = file_record.csv.decode(FILE_ENCODING)
    except UnicodeDecodeError:
        raise ValueError(
            'Cannot decode CSV file (id: {}) as "{}"'
            .format(file_id, FILE_ENCODING)
        )
    else:
        file_sio = io.StringIO(file_str)

    try:
        # create pandas DataFrame from csv string
        df = pd.read_csv(file_sio, skipinitialspace=True)
    except ValueError:
        raise ValueError(
            'pandas.read_csv cannot read CSV file'
        )

    # make column names valid python identifiers
    # PyMC hides variables that end with '_'
    df.columns = df.columns.str.strip(' _')
    df.columns = df.columns.str.replace('[^0-9a-zA-Z_]', '')
    df.columns = df.columns.str.replace('^[^a-zA-Z_]+', '')
    df.columns = pd.Index(
        name if not keyword.iskeyword(name) else ('var_' + name)
        for name in df.columns
    )
    df.columns = pd.Index(
        name if not name == '' else ('var_' + str(i))
        for i, name in enumerate(df.columns)
    )

    # convert 'int' and 'object' columns to 'category'
    for col in df.columns:
        if df[col].dtype == 'object':
            df[col] = df[col].astype('category')
        elif df[col].dtype == 'int':
            df[col] = df[col].astype('category')

    # drop variables apart from category and float
    df = df.select_dtypes(include=['category', 'float'])

    return df


def _create_form_from_df_request(df, request):
    """Accept request object and DataFrame and return WTForm object"""

    # create model form
    form = ModelForm(request.form)

    # add variables to model form options
    _populate_model_form_choices(
        form, df
    )

    return form


def _populate_model_form_choices(form, df):
    """Update NewModelFormBase instance with variable choices"""

    for var_name, var_type in glm.df_to_var_types(df):
        form.response.choices.append((var_name, var_name))
        form.predictors.choices.append((var_name, var_name))

    return


def _create_model_from_form(form, file_id, vars):
    """Create new db.Model object from validate form input"""

    var_dict = dict(vars)

    # create dict of response
    response_name = form.response.data
    response_dict = {
        response_name:
            glm.LIKELIHOOD_FUNCTIONS[var_dict[response_name]]
    }

    # create dict of predictor coefficient priors
    predictors_dict = {}
    for pred_name in form.predictors.data:
        if pred_name != response_name:
            predictors_dict.update({
                pred_name: {
                    None: None  # could populate here with custom priors
                }
            })

    # add the intercept coefficient prior
    predictors_dict.update({
        glm.INTERCEPT_NAME: {
            'normal': {  # could populate here with custom prior
                'mean': form.prior_mean.data,
                'sd': form.prior_sd.data
            }
        }
    })

    # create new Model object
    model = wglmdb.Model(file_id=file_id)
    model.model_name = form.model_name.data
    model.response_json = json.dumps(response_dict)
    model.predictors_json = json.dumps(predictors_dict)
    model.protected = form.protected.data

    return model


def _flash_form_errors(form):
    """Take any errors from form validation and flash them"""

    for field, errors in form.errors.items():
        for error in errors:
            fl.flash(
                'Form Error: {} - {}'.format(
                    getattr(form, field).label.text,
                    error
                )
            )

    return
