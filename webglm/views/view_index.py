"""
view_index.py routines for handling index page data for webglm web-app
"""

import flask as fl
import peewee as pw

import webglm.app as wglm
import webglm.db as wglmdb
import webglm.worker as worker


@wglm.app.route('/', methods=['GET'])
def web_view_index():
    # select all file information
    select_files = (
        wglmdb.File
        .select(
            wglmdb.File.id.alias('file_id'),
            wglmdb.File.file_name,
            wglmdb.File.file_status,

            pw.SQL('null as model_id'),
            pw.SQL('null as model_name'),

            pw.SQL('null as result_id'),
            pw.SQL('null as job_status'),

            wglmdb.File.protected.alias('protected'),
            pw.SQL('"file" as type')
        )
    )

    # select all model info with associated file info
    select_models = (
        wglmdb.File
        .select(
            wglmdb.File.id.alias('file_id'),
            wglmdb.File.file_name,
            wglmdb.File.file_status,

            wglmdb.Model.id.alias('model_id'),
            wglmdb.Model.model_name,

            pw.SQL('null as result_id'),
            pw.SQL('null as job_status'),

            wglmdb.Model.protected.alias('protected'),
            pw.SQL('"model" as type')
        )
        .join(wglmdb.Model, join_type=pw.JOIN.INNER)
    )

    # select all result info with associated file and model info
    select_results = (
        wglmdb.File
        .select(
            wglmdb.File.id.alias('file_id'),
            wglmdb.File.file_name,
            wglmdb.File.file_status,

            wglmdb.Model.id.alias('model_id'),
            wglmdb.Model.model_name,

            wglmdb.Result.id.alias('result_id'),
            wglmdb.Result.job_status,

            wglmdb.Result.protected.alias('protected'),
            pw.SQL('"result" as type')
        )
        .join(wglmdb.Model, join_type=pw.JOIN.INNER)
        .switch(wglmdb.Model)
        .join(wglmdb.Result, join_type=pw.JOIN.INNER)
    )

    # full index query
    index_query = (
        select_files | select_models | select_results
    ).order_by(
        pw.SQL('file_id'),
        pw.SQL('model_id'),
        pw.SQL('result_id')
    )

    # populate ad-hoc tree structure of database objects
    entries = []
    for entry in index_query:
        if entry.type == 'file':
            entries.append((entry, []))
        elif entry.type == 'model':
            entries[-1][1].append((entry, []))
        else:  # entry.type == 'result'
            entries[-1][1][-1][1].append(entry)

    # select busy jobs from result table
    busy_job_query = (
        wglmdb.Result
        .select()
        .where(wglmdb.Result.job_status == wglmdb.RECORD_STATUS['BUSY'])
    )

    # fetch celery task statuses of busy job records
    busy_job_states = {}
    for job in busy_job_query:
        cel_res = worker.do_MCMC.AsyncResult(job.celery_task_id)
        phase, info = cel_res.state, cel_res.info

        if isinstance(info, dict):
            prog = int(100*info['current']/info['total'])
        else:
            prog = None

        busy_job_states[job.id] = {
            'phase': phase,
            'progress': prog
        }

    return fl.render_template(
        'view_index.html',
        entries=entries,
        busy_jobs=busy_job_states
    )
