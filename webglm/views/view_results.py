"""
view_results.py routines for handling results display
"""

import json

import flask as fl
import peewee as pw

import webglm.app as wglm
import webglm.db as wglmdb


@wglm.app.route("/view_results/<int:result_id>", methods=["GET"])
def web_view_results(result_id):
    # Is there a result?
    try:
        result = wglmdb.Result.get(wglmdb.Result.id == result_id)
    except pw.DoesNotExist:
        wglm.notify_log(
            'The results record with id "{}" does not exist.'
            .format(result_id),
            dest=fl.flash, request=fl.request
        )

        return fl.redirect(
            fl.url_for('web_view_index')
        )

    # Has the computation finished?
    if result.job_status != wglmdb.RECORD_STATUS['COMPLETED']:
        wglm.notify_log(
            'The status of the results record with id "{}" is not '
            '"COMPLETED" (status is "{}").'
            .format(result_id, result.job_status),
            dest=fl.flash, request=fl.request
        )

        return fl.redirect(
            fl.url_for('web_view_index')
        )

    plots_dict = json.loads(result.graph_json)

    # Display the data
    return fl.render_template(
        'view_results.html',
        plots_dict=plots_dict
    )
