"""
new_file.py routines for handling file upload for webglm web-app
"""

import flask as fl

import webglm.app as wglm
import webglm.db as wglmdb


@wglm.app.route('/new_file', methods=['POST'])
def web_new_file():
    # has a file been posted?
    if 'file' not in fl.request.files:
        wglm.notify_log(
            'No file has been posted.',
            dest=fl.flash, request=fl.request
        )

        return fl.redirect(
            fl.url_for('web_view_index')
        )

    file = fl.request.files['file']

    if file.filename == '':
        wglm.notify_log(
            'No file has been selected.',
            dest=fl.flash, request=fl.request
        )

        return fl.redirect(
            fl.url_for('web_view_index')
        )

    # create new file record in database
    file_record = wglmdb.File.create(
        file_name=file.filename,
        csv=file.read(),
        file_status=wglmdb.RECORD_STATUS['NEW'],
        protected=bool(fl.request.form.get('protected', False))
    )

    wglm.notify_log(
        'File "{}" has been uploaded.'
        .format(file_record.file_name),
        dest=fl.flash, request=fl.request
    )

    return fl.redirect(
        fl.url_for('web_view_index')
    )
