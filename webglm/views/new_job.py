"""
new_job.py routines for handling job creation for webglm web-app
"""

import flask as fl
import peewee as pw
import wtforms as wtf

import webglm.app as wglm
import webglm.db as wglmdb
import webglm.glmdata as glm
import webglm.worker as worker


class NewJobForm(wtf.Form):
    """WTForms Form class for new job creation"""

    steps = wtf.fields.IntegerField(
        'Number of steps',
        [wtf.validators.DataRequired('Enter the number of steps to simulate')],
        default="2000"
    )

    burn_steps = wtf.fields.IntegerField(
        'Number of steps to burn',
        [wtf.validators.DataRequired('Enter the number of steps to burn')],
        default="200"
    )

    thinning_factor = wtf.fields.IntegerField(
        'Thinning Factor',
        [wtf.validators.DataRequired('Enter the thinning factor')],
        default="2"
    )

    step_type = wtf.fields.SelectField(
        'MCMC Step Type',
        [wtf.validators.AnyOf(list(glm.STEP_METHODS.keys()))],
        default=glm.STEP_METHODS.pop('_default_key'),
        choices=list(glm.STEP_METHODS.items()),
        option_widget=None
    )

    protected = wtf.fields.BooleanField(
        'Protect from deletion',
        default=False
    )

    def validate_steps(form, field):
        MAX_STEPS = wglm.app.config['MAX_STEPS']
        if field.data <= 0 or field.data >= MAX_STEPS:
            raise wtf.ValidationError(
                'Number of steps must be between 0 and ' +
                str(MAX_STEPS)
            )

    def validate_burn_steps(form, field):
        if field.data <= 0 or field.data >= form.steps.data:
            raise wtf.ValidationError(
                'Number of burn steps must be between 0 '
                'and the number of steps'
            )

    def validate_thinning_factor(form, field):
        if field.data <= 0:
            raise wtf.ValidationError(
                'Thinning factor must be greater than zero'
            )


@wglm.app.route('/new_job/<int:model_id>', methods=['GET', 'POST'])
def web_new_job(model_id):
    try:
        # fetch model record by model_id
        model = wglmdb.Model.get(wglmdb.Model.id == model_id)
    except pw.DoesNotExist:
        wglm.notify_log(
            'The Model with id "{}" does not exist.'
            .format(model_id),
            dest=fl.flash, request=fl.request
        )

        return fl.redirect(
            fl.url_for('web_view_index')
        )

    # create WTForms form object
    form = NewJobForm(fl.request.form)

    if fl.request.method == 'POST' and form.validate():

        # create result record and configure job
        result = wglmdb.Result(model_id=model_id)
        result.steps = form.steps.data
        result.burn_steps = form.burn_steps.data
        result.thinning_factor = form.thinning_factor.data
        result.step_type = form.step_type.data
        result.job_status = wglmdb.RECORD_STATUS['NEW']
        result.protected = form.protected.data
        result.save()

        # launch job
        celery_result = worker.do_MCMC.delay(result.id)
        result.celery_task_id = celery_result.id
        result.save()

        wglm.notify_log(
            'The job with id "{}" has been scheduled for execution.'
            .format(result.id),
            dest=fl.flash, request=fl.request
        )

        return fl.redirect(
            fl.url_for('web_view_index')
        )

    else:  # http GET or invalid form POSTed

        # flash any errors in the form
        if len(form.errors) > 0:
            _flash_form_errors(form=form)

        return fl.render_template(
            'new_job.html',
            form=form,
            model=model
        )


def _flash_form_errors(form):
    """Take any errors from form validation and flash them"""

    for field, errors in form.errors.items():
        for error in errors:
            fl.flash(
                'Form Error: {} - {}'.format(
                    getattr(form, field).label.text,
                    error
                )
            )
