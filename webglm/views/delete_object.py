"""
delete_object.py routines for handling object deletion in webglm web-app
"""

import flask as fl
import peewee as pw

import webglm.app as wglm
import webglm.db as wglmdb


@wglm.app.route(
    '/delete_object/<string:object_type>/<int:id>',
    methods=['GET']
)
def web_delete_object(object_type, id):
    DELETABLE_TABLES = {
        'file': wglmdb.File,
        'model': wglmdb.Model,
        'result': wglmdb.Result
    }

    # only tables in DELETABLE_TABLES may be deleted from
    if object_type not in DELETABLE_TABLES:
        wglm.notify_log(
            'Objects of type "{}" cannot be deleted.'
            .format(object_type),
            dest=fl.flash, request=fl.request
        )

        return fl.redirect(
            fl.url_for('web_view_index')
        )

    Table = DELETABLE_TABLES[object_type]
    table_name = Table._meta.db_table

    # try to fetch object from valid Table
    try:
        record = Table.get(Table.id == id)
    except pw.DoesNotExist:
        wglm.notify_log(
            'The record with id "{}" does not exist in table "{}".'
            .format(id, table_name),
            dest=fl.flash, request=fl.request
        )

        return fl.redirect(
            fl.url_for('web_view_index')
        )

    # cannot delete protected records
    if record.protected:
        wglm.notify_log(
            'The record with id "{}" in table "{}" is protected. '
            'This may be for debugging purposes.'
            .format(id, table_name),
            dest=fl.flash, request=fl.request
        )

        return fl.redirect(
            fl.url_for('web_view_index')
        )

    # delete record
    record.delete_instance()

    wglm.notify_log(
        'Deleted row with id "{}" from table "{}".'
        .format(id, table_name),
        dest=fl.flash, request=fl.request
    )

    return fl.redirect(
        fl.url_for('web_view_index')
    )
