"""
test_webglm.py unit tests for webglm application.
"""

import os
import tempfile
import functools
import importlib

import webglm.main as wglm
import webglm.db as wglmdb

# tests are assumed to run via run_tests.sh script in root directory
TEST_PATH = './webglm/tests/'
TEST_FILENAME = '10 test data: beta_0 = 3, beta_x = 5, sigma = 2.csv'
TEST_MODELNAME = 'testing model'


class AppTestContext:
    """Create context with temprary database and flask test client"""
    def __init__(self):
        self.test_client = None

    def __enter__(self):
        """Create temporary database and return flask test client"""
        if wglm.app.config['DATABASE_TYPE'] != 'sqlite':
            raise NotImplementedError(
                'Test context not implemented for non-sqlite databases'
            )

        # patch webglm app object to use temporary database
        self.db_fd, wglm.app.config['DATABASE'] = tempfile.mkstemp()

        # reload database with new configuration
        importlib.reload(wglmdb)

        # initialise test database
        wglmdb.db.drop_tables(
            [wglmdb.File, wglmdb.Model, wglmdb.Result],
            safe=True
        )
        wglmdb.db.create_tables(
            [wglmdb.File, wglmdb.Model, wglmdb.Result]
        )

        # create test client
        wglm.app.config['TESTING'] = True
        self.test_client = wglm.app.test_client()

        return self.test_client

    def __exit__(self, *args):
        """Close and delete temporary database file"""
        os.close(self.db_fd)
        os.unlink(wglm.app.config['DATABASE'])

        return


def _post_new_file(test_client,
                   filename=TEST_FILENAME,
                   protected=False):
    with open(TEST_PATH + TEST_FILENAME, mode='rb') as f:
        # 'upload' some test data
        resp = test_client.post(
            '/new_file',
            data=dict(
                file=(f, TEST_FILENAME),
                protected='' if not protected else 'True'
            ),
            follow_redirects=True
        )

    return resp


def _post_new_model(test_client,
                    response='y',
                    modelname=TEST_MODELNAME,
                    protected=False):
    resp = test_client.post(
        '/new_model/1',
        data=dict(
            response=str(response),
            predictors=['x'],
            prior_mean='0',
            prior_sd='1e10',
            model_name=TEST_MODELNAME,
            protected='' if not protected else 'True'
        ),
        follow_redirects=True
    )

    return resp


def test_empty():
    """Test for empty index"""
    with AppTestContext() as test_client:
        resp = test_client.get('/')

        assert b'No files' in resp.data


def test_upload_file():
    """Test file upload"""
    with AppTestContext() as test_client:
        resp = _post_new_file(test_client)

        assert bytes(TEST_FILENAME, 'utf-8') in resp.data


def test_upload_protected_file():
    """Test protected file upload"""
    with AppTestContext() as test_client:
        resp = _post_new_file(test_client, protected=True)

        assert b'fa-lock' in resp.data


def test_delete_file():
    """Test file delete"""
    with AppTestContext() as test_client:
        _post_new_file(test_client)

        # delete test data
        resp = test_client.get(
            '/delete_object/file/1',
            follow_redirects=True
        )

        assert b'Deleted row with id' in resp.data


def test_bad_delete_fails():
    """Test that deleting a nonexistent file fails"""
    with AppTestContext() as test_client:
        resp = test_client.get(
            '/delete_object/file/1',
            follow_redirects=True
        )

        assert b' does not exist in table ' in resp.data


def test_delete_protected_file():
    """Test protected file delete fails"""
    with AppTestContext() as test_client:
        _post_new_file(test_client, protected=True)

        # try to delete test data
        resp = test_client.get(
            '/delete_object/file/1',
            follow_redirects=True
        )

        assert b'is protected. ' in resp.data


def test_new_model():
    """Test new model creation"""
    with AppTestContext() as test_client:
        _post_new_file(test_client)
        resp = _post_new_model(test_client)

        assert (
            bytes(TEST_MODELNAME, 'utf-8') in resp.data and
            b'webglm Objects' in resp.data
        )


def test_bad_new_model():
    """Test bad model fails to create"""
    with AppTestContext() as test_client:
        _post_new_file(test_client)
        resp = _post_new_model(test_client, response='blah')

    assert (
        b'Form Error' in resp.data and
        b'Model Configuration' in resp.data
    )
