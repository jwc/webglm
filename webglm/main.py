"""
main.py main entry-point for webglm application
"""

import logging
import os

import webglm.app as wglm
import webglm.db as wglmdb

# load web-application views
import webglm.views.view_index
import webglm.views.new_file
import webglm.views.delete_object
import webglm.views.new_model
import webglm.views.new_job
import webglm.views.view_results

app = wglm.app


# Open database connection before each request
@wglm.app.before_request
def connect_database():
    wglmdb.db.connect()


@wglm.app.before_request
def add_file_handler():
    file_handler = logging.FileHandler(wglm.app.config['LOG'])
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(wglm.log_formatter)

    wglm.logger.addHandler(file_handler)


# Close database connection after each request
@wglm.app.after_request
def close_database(response):
    wglmdb.db.close()
    return response


@wglm.app.after_request
def close_file_handler(response):
    wglm.logger.handlers[0].flush()
    wglm.logger.handlers[0].close()
    wglm.logger.removeHandler(wglm.logger.handlers[0])
    return response


@wglm.app.cli.command('initdb')
def command_initdb():
    """Flask CLI to initialise the database"""
    init_db_and_logs()
    print("Initialised database %s for webglm" % wglm.app.config['DATABASE'])


def init_db_and_logs():
    """Initialise the database"""
    # create database and log directories if nonexistent
    _setup_db_log_dirs()

    wglmdb.db.drop_tables(
        [wglmdb.File, wglmdb.Model, wglmdb.Result],
        safe=True
    )

    wglmdb.db.create_tables(
        [wglmdb.File, wglmdb.Model, wglmdb.Result]
    )

    return


def _setup_db_log_dirs():
    """Create directories which contain the sqlite database and logs"""
    for dirname in ['db', 'logs']:
        dirpath = os.path.join(wglm.app.root_path, dirname)
        if not os.path.exists(dirpath):
            os.makedirs(dirpath)

    return
