webglm
=======

**webglm** is a web-app demonstration. It is able to perform Markov Chain Monte Carlo (<a href="https://en.wikipedia.org/wiki/Markov_chain_Monte_Carlo">MCMC</a>) simulation of bayesian generalised linear models (<a href="https://en.wikipedia.org/wiki/Generalized_linear_model">GLM</a>), which are configured from user-uploaded csv datafiles.

**webglm** is written in Python 3 and uses several open-source packages, including:

- <a href="http://flask.pocoo.org">Flask</a> and <a href="http://jinja.pocoo.org">Jinja2</a> for the web interface
- <a href="http://peewee-orm.com">peewee</a> for Object Relational Mapping
- <a href="http://pandas.pydata.org">pandas</a> for dataset manipulation
- <a href="http://www.celeryproject.org">Celery</a> for background task execution
- <a href="https://github.com/pymc-devs/pymc3">PyMC3</a> and <a href="http://deeplearning.net/software/theano/">Theano</a> for MCMC simulation
- <a href="http://mpld3.github.io">mpld3</a> for web-based interactive graphing from within python
